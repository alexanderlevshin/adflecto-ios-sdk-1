//
//  ViewController.m
//  SimpleDemo
//
//  Created by Artem on 18.05.15.
//  Copyright (c) 2015 Adflecto. All rights reserved.
//

#import "SimpleDemoViewController.h"
#import "Adflecto/AdflectoVideoViewController.h"

@interface SimpleDemoViewController () <AdflectoVideoViewControllerDelegate> {

    IBOutlet UITextView *resultView;
    
}

@end

@implementation SimpleDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSLog(@"NSStringFromUIEdgeInsets(resultView.contentInset) = %@", NSStringFromUIEdgeInsets(resultView.contentInset));
//    resultView.contentOffset = nil;
    resultView.contentOffset = CGPointMake(0.0f, 0.0f);
//    [resultView setTextAlignment:NSTextAlignmentRight];
}

- (IBAction)showAd:(id)sender {
    AdflectoVideoViewController *vc = [[AdflectoVideoViewController alloc] initWithDelegate:self withViewController:self];
    [vc startWithPlaceId:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)adflectoFinishedWithResult:(AdflectoResult)result errorCode:(AdflectoErrorCode)errorCode errorMessage:(NSString *)errorMessage {
    switch (result) {
        case AdflectoError: {
            NSString *message = @"An ad was not shown because of error: ";
            [resultView setText: [message stringByAppendingString: errorMessage]];
            break;
        }
        case AdflectoSuccess:
            [resultView setText: @"An ad was sucessfully shown until the end"];
            break;
        case AdflectoPostpone:
            [resultView setText: @"An ad show was postponed because of missing pre-cached media file"];
            break;
        case AdflectoNoBanner:
            [resultView setText: @"An ad was not shown because of no demand"];
            break;
        case AdflectoClick:
            [resultView setText: @"An ad was shown and clicked"];
            break;
        case AdflectoClose:
            [resultView setText: @"An ad was shown and closed by user"];
            break;
    }
}

- (void)adflectoVideoCachedInBackground {
    dispatch_sync(dispatch_get_main_queue(), ^{
        [resultView setText: @"Media file was sucessfully downloaded and cached. Press 'Show Ad' button again to watch an ad"];
    });
}

@end
