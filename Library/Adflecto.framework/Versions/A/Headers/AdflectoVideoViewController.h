#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/*!
 * @class AdflectoVideoViewController
 *
 * @discussion
 * Loads VAST XML data from backend and plays AD video.
 * AD showing result is passed to AdflectoVideoViewControllerDelegate.
 */
@class AdflectoVideoViewController;

typedef NS_ENUM(NSUInteger, AdflectoResult) {
    /** An ad wasn't shown because of any errors */
            AdflectoError = -1,

    /** An ad was successfully shown and viewed until its end */
            AdflectoSuccess = 0,

    /** An ad showing was postponed because of slow network. Media file is downloading in a background mode */
            AdflectoPostpone = 1,

    /** An ad wasn't shown because of no demand */
            AdflectoNoBanner = 2,

    /** An ad was successfully shown and user clicked on it */
            AdflectoClick = 3,

    /** An ad was successfully shown but was interrupted by user */
            AdflectoClose = 4

};

typedef NS_ENUM(NSUInteger, AdflectoErrorCode) {

    AdflectoErrorNone = 0,
    AdflectoErrorXMLParse = 1,
    AdflectoErrorSchemaValidation = 2,
    AdflectoErrorTooManyWrappers = 3,
    AdflectoErrorVideoPlayback = 4,
    AdflectoErrorNoNetwork = 5,
    AdflectoErrorPlayerNotReady = 6,
    AdflectoErrorTimeout = 7,
    AdflectoErrorPlayerHung = 8,
    AdflectoErrorMovieTooShort = 9,
    AdflectoErrorNoCompatibleMediaFile = 10,
    AdflectoErrorVastServerError = 11,
    AdflectoErrorException = 12
};

/*!
 * @delegate AdflectoVideoViewControllerDelegate
 *
 * @discussion
 * Delegate which receives AD showing result.
 */
@protocol AdflectoVideoViewControllerDelegate <NSObject>

@required

/*!
* @method -adflectoFinishedWithResult:errorCode:errorMessage
*
* @param result
* AD showing result
*
* @param errorCode
* an error code
*
* @param errorMessage
* descriptive error message
*
* @discussion
* Called after AD module has finished its work.
*/
- (void)adflectoFinishedWithResult:(AdflectoResult)result errorCode:(AdflectoErrorCode)errorCode errorMessage:(NSString *)errorMessage;

/*!
* @method -adflectoVideoCachedInBackground
*
* @discussion
* Called when AD module has finished video download background task.
*/
- (void)adflectoVideoCachedInBackground;

@optional

@end

@interface AdflectoVideoViewController : UIViewController

/*!
* @method -initWithDelegate:withViewController:
*
* @param delegate
* delegate which receives AD showing result.
*
* @param viewController
* presenting view controller.
*
* @discussion
* Designated initializer
*/
- (id)initWithDelegate:(id <AdflectoVideoViewControllerDelegate>)delegate withViewController:(UIViewController *)viewController;

/*!
* @method -startWithPlaceId
*
* @param placeId
* AD place id
*
* @discussion
* Passes control to AD module.
*/
- (void)startWithPlaceId:(NSInteger)placeId;

@end
